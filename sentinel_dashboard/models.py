from sentinel_dashboard import db, login_manager
from flask.ext.login import UserMixin

class User(db.Document, UserMixin):
    '''
    The model for users collection in mongodb
    '''
    username = db.StringField(unique=True, required=True)
    password = db.StringField(required=True)
    isAdmin = db.BooleanField(required=True)
    ownedWebsites = db.ListField(db.StringField())
    dateCreated = db.DateTimeField()

    def get_user(self, username):
        '''
        Returns: user object or None
        Parameter: username (string)

        The function queries for the username in the db and returns
        the user object if query is successful.
        Else it will return None.
        '''
        if not username:
            raise ValueError()

        user_found = User.objects(username=username)

        if len(user_found) > 0:
            return user_found[0]
        elif len(user_found) == 0:
            return None
        else:
            raise Exception('Database integrity error.')

@login_manager.user_loader
def user_loader(user_id):
    

class ZapReport(db.Document):
    '''
    Model for ZAP report
    '''
    report = db.DynamicField()
