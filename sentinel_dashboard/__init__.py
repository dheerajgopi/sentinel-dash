# Importing flask and related modules
from flask import Flask, render_template
from flask.ext.login import LoginManager

# Importing mongoengine
from flask.ext.mongoengine import MongoEngine

# Defining WSGI app object
app = Flask(__name__)

# Loading configuration file
app.config.from_object('config')

# defining mongoengine database object
db = MongoEngine(app)

# Associating LoginManager to current app
login_manager = LoginManager()
login_manager.init_app(app)

@app.errorhandler(404)
def err_not_found(error):
    '''
    handle 404 error
    '''
    return render_template('404.html'), 404

# Importing blueprints
from sentinel_dashboard.mod_auth.controllers import mod_auth
from sentinel_dashboard.mod_home.controllers import mod_home

# Registering blueprints
app.register_blueprint(mod_auth)
app.register_blueprint(mod_home)
