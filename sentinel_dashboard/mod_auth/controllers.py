from flask import Blueprint, request, render_template, redirect, url_for, \
    flash

from flask.ext.login import login_user

# Import password encryption tools
from werkzeug import generate_password_hash, check_password_hash

# Import database object from app
from sentinel_dashboard import db

# Import forms
from sentinel_dashboard.mod_auth.forms import LoginForm

# Import models
from sentinel_dashboard.models import User

# defining blueprint of auth module
mod_auth = Blueprint('auth', __name__, url_prefix='/',
                     template_folder='./../templates/auth')

# Set routes
@mod_auth.route('/', methods=['GET', 'POST'])
def login_page():
    '''
    Displays login page, and handles login form
    '''
    # If login form is submitted, get the form data
    form = LoginForm(request.form)

    # Verify login form
    if form.validate_on_submit():
        user = User().get_user(form.username.data)
        if not user:
            flash('wrong username or password', 'error')
            return render_template('signin.html', loginform=form)

        elif user.password != form.password.data:
            flash('wrong username or password', 'error')
            return render_template('signin.html', loginform=form)

        else:
            user.authenticated = True
            login_user(user)
            return redirect(url_for('home.show_homepage'))

    return render_template('signin.html', loginform=form)
