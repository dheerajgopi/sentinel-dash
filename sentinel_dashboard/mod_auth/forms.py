from flask.ext.wtf import Form
from wtforms import TextField, PasswordField
from wtforms.validators import Required

class LoginForm(Form):
    '''
    User login form. Contains username and password field.
    '''
    username = TextField('Username', [Required(message='Username is required')])
    password = PasswordField('Password', [Required(message='password is required')])
