from flask import Blueprint, request, render_template, redirect, url_for

from flask.ext.login import login_required

from sentinel_dashboard import db

from sentinel_dashboard.models import ZapReport

# Defining the blueprint
mod_home = Blueprint('home', __name__, url_prefix='/home',
                     template_folder='./../templates/home')

@mod_home.route('/', methods=['GET', 'POST'])
@login_required
def show_homepage():
    '''
    Shows the homepage. Requires login.
    '''
    return render_template('home.html')
