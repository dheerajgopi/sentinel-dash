#!env/bin/python

from sentinel_dashboard import app

# Run the server
app.run(host='127.0.0.1', port=8080, debug=True)
