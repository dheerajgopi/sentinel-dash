import os

# Enable development environment
DEBUG = True

# Set base directory for the application
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Database settings
MONGODB_DB = 'sentinel-dashboard'
MONGODB_HOST = '127.0.0.1'
MONGODB_PORT = 27017

# Application Threads = 2. One to handle incoming requests and another to perform background ops.
THREADS_PER_PAGE = 2

# Enable CSRF
CSRF_ENABLED = True
SRF_SESSION_KEY = 'secret'

# secret key for cookies
SECRET_KEY = 'secret'
